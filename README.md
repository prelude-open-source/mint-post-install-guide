# Mint Post install guide

This guide shows how to set up linux mint in a nice way so that it is awesome for python and react. And docker of course.

## sort out your workspaces and keyboard shortcuts

In mint, workspaces should be turned on by default. yay. Press Ctrl+Alt+Right to make sure

Set up sensible keyboard shortcuts so you can move windows around and switch between workspaces easily.

It's nice to do this first because it makes everything else less painful.

## Install Chrome

Just google it

## install some basic things and prerequisites and stuff

```[bash]
sudo apt-get update && sudo apt-get upgrade
sudo apt-get dist-upgrade
sudo apt-get install git terminator openssh-client openssh-server gitk tree
sudo apt-get install build-essential python-dev python-setuptools python-pip python-smbus
sudo apt-get install libncursesw5-dev libgdbm-dev libc6-dev
sudo apt-get install zlib1g-dev libsqlite3-dev tk-dev
sudo apt-get install libssl-dev openssl
sudo apt-get install libffi-dev
```

## upgrade your shell

This is not required but does make life quite a lot easier.

Install zsh or an alternative. The instructions below are zsh specific. Lots of people prefer fish-shell and oh-my-fish.

```[bash]
sudo apt-get install zsh
```

Then follow the instructions [here](https://github.com/robbyrussell/oh-my-zsh)

Oh-my-zsh comes with loads of cool plugins. But the list is not complete. Do the following:

```[bash]
cd ~/.oh-my-zsh/plugins/
git clone https://github.com/djui/alias-tips.git
```

edit your .zshrc to include the following plugins. Feel free to personalise this list:

```[zsh]
plugins = (
  git docker docker-compose per-directory-history alias-tips
)
```

You'll also notice that the default theme is `robbyrussell`. It's a fine theme but there are many others. You can also make your own if you feel so inclined.

## Install the latest Python

A guide for insalling python3.7: https://tecadmin.net/install-python-3-7-on-ubuntu-linuxmint/

then:

```[bash]
sudo apt install python-pip
sudo apt install python3-pip
```

## virtualenvwrapper

```[bash]
sudo -H pip install virtualenvwrapper
```

There are a few newer alternatives to this but I've found them to be a lil unstable and annoying.

You'll need to update your `.zshrc` or your `.bashrc` depending on whether you are using zsh or bash.

In a terminal:

```[bash]
nano ~/.zshrc
```

Then paste this in at the top of the file:

```[zsh]
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/workspace
source /usr/local/bin/virtualenvwrapper.sh
```

## node

Install the latest LTS relelease. There are some instructions [here](https://tecadmin.net/install-latest-nodejs-npm-on-linux-mint/)

Also create-react-app is super useful in my life.  Look [here](https://github.com/facebook/create-react-app)

## docker and docker-compose

You can mostly follow [these instructions](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

Except: when adding the repository do this:

```[bash]
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release; echo "$UBUNTU_CODENAME") stable"
```

Make sure to follow the "next steps". We dont want to need `sudo` whenever we run stuff. There are some security concerns but for development purposes it's nice to make life convenient.

Next up install docker-compose

## vscode

You don't have to use vscode, but it is quite nice. Get it [here](https://code.visualstudio.com/download)

The following plugins are recommended for a start:

- Sublime Text Keymap and Settings Importer
- ESLint
- Python

## slack

[We like slack](https://slack.com/downloads/linux)

## gcloud shell

A lot of our stuff lives on google cloud. So [this](https://cloud.google.com/sdk/docs/quickstart-linux) is handy.

## gitlab and github keys

Having to enter your usename and password every time you interact with git stuff is a pain. You can create one ssh key and use it for both services. Secure your key with a passphrase.

- [Gitlab guide](https://docs.gitlab.com/ee/ssh/)
- [Github guide](https://help.github.com/en/articles/adding-a-new-ssh-key-to-your-github-account)

## Some other useful guides

- [how-to-install-software-in-ubuntu-linux-a-complete-guide-for-newbie](https://www.ubuntupit.com/how-to-install-software-in-ubuntu-linux-a-complete-guide-for-newbie/)
- [best-things-to-do-after-installing-linux-mint](https://www.ubuntupit.com/best-things-to-do-after-installing-linux-mint/)
